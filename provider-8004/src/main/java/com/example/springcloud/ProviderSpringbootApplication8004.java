package com.example.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
@EnableEurekaClient
public class ProviderSpringbootApplication8004 {

    public static void main(String[] args) {
        SpringApplication.run(ProviderSpringbootApplication8004.class,args);
    }
}
