package com.example.springcloud.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Dept {

    private Integer deptNo;

    private String deptName;

    private String dbSource;
}
