package com.example.springcloud.provider.service.impl;

import com.example.springcloud.common.entity.Dept;
import com.example.springcloud.provider.mapper.DeptMapper;
import com.example.springcloud.provider.service.DeptService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Resource
    private DeptMapper deptMapper;
    @Override
    public Dept get(Integer deptNo) {
        return deptMapper.selectByPrimaryKey(deptNo);
    }
    @Override
    public List<Dept> selectAll() {
        return deptMapper.GetAll();
    }
}
