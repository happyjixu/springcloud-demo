package com.example.springcloud.provider.service;

import com.example.springcloud.common.entity.Dept;
import java.util.List;

public interface DeptService {

    Dept get(Integer deptNo);

    List<Dept> selectAll();
}
