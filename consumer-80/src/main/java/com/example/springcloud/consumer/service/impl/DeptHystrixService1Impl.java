package com.example.springcloud.consumer.service.impl;

import com.example.springcloud.consumer.service.DeptHystrixService1;
import org.springframework.stereotype.Service;

@Service
public class DeptHystrixService1Impl implements DeptHystrixService1 {
    @Override
    public String deptInfo_Ok(Integer id) {
        return "--------------------C语言中文网提醒您，系统繁忙，请稍后重试！（deptInfo_Ok解耦回退方法触发）-----------------------";
    }

    @Override
    public String deptInfo_Timeout(Integer id) {
        return "--------------------C语言中文网提醒您，系统繁忙，请稍后重试！（deptInfo_Timeout解耦回退方法触发）-----------------------";
    }
}
