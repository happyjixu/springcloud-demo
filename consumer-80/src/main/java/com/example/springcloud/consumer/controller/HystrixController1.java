package com.example.springcloud.consumer.controller;

import com.example.springcloud.consumer.service.DeptHystrixService1;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@Slf4j
@RestController
public class HystrixController1 {

    @Resource
    private DeptHystrixService1 deptHystrixService;

    @RequestMapping(value = "/consumer/dept/hystrix1/ok/{id}")
    public String deptInfo_Ok(@PathVariable("id") Integer id) {
        return deptHystrixService.deptInfo_Ok(id);
    }

    @RequestMapping(value = "/consumer/dept/hystrix1/timeout/{id}")
    public String deptInfo_Timeout(@PathVariable("id") Integer id) {
        String s = deptHystrixService.deptInfo_Timeout(id);
        log.info(s);
        return s;
    }

}
