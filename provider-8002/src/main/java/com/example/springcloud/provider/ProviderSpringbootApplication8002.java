package com.example.springcloud.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.example.springcloud.provider.mapper"})
public class ProviderSpringbootApplication8002 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderSpringbootApplication8002.class,args);
    }
}
