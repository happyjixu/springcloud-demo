package com.example.springcloud.provider.mapper;

import com.example.springcloud.common.entity.Dept;
import java.util.List;

public interface DeptMapper {

    //根据主键获取数据
    Dept selectByPrimaryKey(Integer deptNo);

    //获取表中的全部数据
    List<Dept> GetAll();

}
